﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class fading : MonoBehaviour {

	public Texture2D fadeOutTexture; 	//the texture that will overlay the screen. This can be a black image or a loading graphic. 
	public float fadeSpeed=0.8f; 		//the fading speed

	private int drawDepth=-1000;		//the texture's order in the draw-hierarchy; a low number means that it will render on top
	private float alpha=1.0f;			//the texture's alpha-value
	private int fadeDir=-1;				//the direction to fade: in=-1, out=1

	void onGUI () {
		//fade out/in alpha using a direction, speed, time.deltatime to convert the operation to seconds
		alpha+=fadeDir*fadeSpeed*Time.deltaTime;
		//force (clamp) the number between 0 and 1 because GUI.color uses alpha values between 0 and 1
		alpha=Mathf.Clamp01(alpha);

		//set color of our GUI (in this case the texture). All color values remain the same and the Alpha is set to the alpha variable
		GUI.color=new Color(GUI.color.r,GUI.color.g,GUI.color.b,alpha);		//set the alpha value
		GUI.depth=drawDepth;												//make the black texture render on top (drawn last)
		GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),fadeOutTexture); //draw the texture to fit the entire screen area
	}

	//set fadeDir to the direction parameter making the scene fade in if -1 and out if 1
	public float BeginFade(int direction){
		fadeDir = direction;
		return (fadeSpeed); //do this so it's easy to time the application.loadlevel()
	}

	//onlevelwasloaded is called when a level is loaded. it takes loaded level index (int) as a parameter so you can limit the fade in to certain scenes
	void OnLevelWasLoaded(){
		//alpha=1; //use this if the alpha is not set to 1 by default
		BeginFade (-1);	//call the fadein function
	}

	void OnEnable(){
		//Tell our 'OnLevelFinishedLoading' function to start listening fo a scene change as soon as this script is enabled. 
		//SceneManager.sceneLoaded += OnLevelFinishedLoading;
	}

	void OnDisable(){
		//Tell our 'OnLevelFinishedLoading' function to stop listening fo a scene change as soon as this script is enabled. Remember to always have an unsubscription fo every delegate you subscribe to!
		//SceneManager.sceneLoaded -= OnLevelFinishedLoading;
	}

	void OnLevelFinishedLoading(Scene giant_donut, LoadSceneMode Single){
		Debug.Log("Level loaded");
		//Debug.Log(Scene.name);
	}


}
