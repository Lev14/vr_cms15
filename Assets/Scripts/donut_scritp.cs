﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class donut_scritp : MonoBehaviour {


//----------DEFINING-VARIABLES--------------------------------------------------------------------------------

	static int counter=0;
	public GameObject bacteria_1;
	public GameObject bacteria_2;
	public GameObject bacteria_3;
	public GameObject bacteria_4;
	public GameObject bacteria_5;
	public WindZone windzone_donut;





//----------STANDARD-FUNCTIONS-------------------------------------------------------------------------------

	// Use this for initialization
	void Start () {
		
		counter = counter + 1;

		ParticleSystem ps2=bacteria_2.GetComponent<ParticleSystem>();
		var em2 = ps2.emission;
		em2.enabled = false; 

		ParticleSystem ps3=bacteria_3.GetComponent<ParticleSystem>();
		var em3 = ps3.emission;
		em3.enabled = false; 

		ParticleSystem ps4=bacteria_4.GetComponent<ParticleSystem>();
		var em4 = ps4.emission;
		em4.enabled = false; 

		ParticleSystem ps5=bacteria_5.GetComponent<ParticleSystem>();
		var em5 = ps5.emission;
		em5.enabled = false; 



		if (counter == 1) { //folgende Aktionen passieren, sobald der User das erste mal hinsieht
			Debug.Log("Donut-Counter ist 1");

			//komische Struktur von dem ParticleSystem...

			StartCoroutine("getBackToLaboratory",4);

		} //end of if
		else if (counter == 2) {
			Debug.Log("Donut-Counter ist 2");
			StartCoroutine("setActiveWindAndBacteriaSources");
			StartCoroutine("getBackToLaboratory",10);
		} //end of else if 1
		else if (counter == 3) {
			Debug.Log("Donut-Counter ist 3");
			StartCoroutine("AttackOfBacteria");
			StartCoroutine("getBackToLaboratory",10);
		}//end of else if 2


		//Debug.Log ("ich bin gestartet!");

		Scene scene = SceneManager.GetActiveScene ();
		//Debug.Log (scene.name); //have to take 'scene.name' because 'scene' is a GameObject



	}	//end of void Start


//----------COROUTINES----------------------------------------------------------------------------------------------------

	IEnumerator getBackToLaboratory(int waitTime){
		yield return new WaitForSeconds(waitTime); //stay in donut-world for some time
		//leave donut-world
		//Debug.Log("warten vorbei");
		SceneManager.LoadSceneAsync ("laboratory", LoadSceneMode.Single);
	}	//end of getBackToLaboratory


	IEnumerator setActiveWindAndBacteriaSources(){
		//TODO: the two other bacteria sources currently have 0 emission. I want to set it to 20 but somehow it doesn't work...
		windzone_donut.windMain=10.0f;	//increase wind strength
		windzone_donut.windTurbulence=2;
		windzone_donut.windPulseMagnitude = 2;
		windzone_donut.windPulseFrequency = 5;

		//activate second bacteria source
		ParticleSystem ps2=bacteria_2.GetComponent<ParticleSystem>();
		var em2 = ps2.emission;
		em2.enabled = true; 


		//Debug.Log ("wind stronger");
		yield return new WaitForSeconds(6);

		//activate third bacteria source
		ParticleSystem ps3=bacteria_3.GetComponent<ParticleSystem>();
		var em3 = ps3.emission;
		em3.enabled = true; 
		//Debug.Log ("second source on");

		yield return new WaitForSeconds(1);


	}	//end of setActiveWindAndBacteriaSources


	IEnumerator AttackOfBacteria(){
		windzone_donut.windMain=15.0f;	//increase wind strength
		windzone_donut.windTurbulence=3;
		windzone_donut.windPulseMagnitude = 3;
		windzone_donut.windPulseFrequency = 7;

		ParticleSystem ps2=bacteria_2.GetComponent<ParticleSystem>();
		var em2 = ps2.emission;
		em2.enabled = false; 

		ParticleSystem ps3=bacteria_3.GetComponent<ParticleSystem>();
		var em3 = ps3.emission;
		em3.enabled = true; 

		ParticleSystem ps4=bacteria_4.GetComponent<ParticleSystem>();
		var em4 = ps4.emission;
		em4.enabled = true; 

		ParticleSystem ps5=bacteria_5.GetComponent<ParticleSystem>();
		var em5 = ps5.emission;
		em5.enabled = true; 
		//Debug.Log ("second source on");




		//Debug.Log ("wind strongest");
		yield return new WaitForSeconds(1);
		//TODO: Here the start-speed should be increased.
		//first_bacteria_source.startSpeed = 200;
	}	//end of AttackOfBacteria

}	//end of class
