﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "ZP_Hub")
        {
            SceneManager.LoadScene("ZP_Exp1");
        }
        else
            SceneManager.LoadScene("ZP_Hub");
    }
}