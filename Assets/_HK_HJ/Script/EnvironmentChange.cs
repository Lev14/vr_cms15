﻿// Copyright 2016 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;

public class EnvironmentChange : MonoBehaviour {
	public StereoController mainCamera;
	public GvrAudioSource spaceInvadersSound;
	public MeshRenderer spaceInvaders;

	Color activeColor = new Color(3,3,3);

	void Update () {
		RaycastHit hit;
		Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out hit);
		Collider collider = hit.collider;
		if (collider != null) {
			if (collider.tag == "Monitor") {
				spaceInvaders.material.SetColor ("_Color", activeColor);
			}
		} else {
			if (spaceInvaders.material.color == activeColor) {
				spaceInvaders.material.SetColor ("_Color", Color.white);
			}
		}
	}
}
