﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class Boundary{
	public int zMin, zMax;
}

public class PlayerController : MonoBehaviour{

	public Boundary boundary;

	public GameObject arrow;
	public Transform arrowSpawn;

	float delay = 0.5f;
	private float lastShot;

	public delegate void DeathAction();
	public static event DeathAction OnPlayerDead;

	void OnEnable(){
		GameLogic.OnDarkSideChanged += StopGame;
	}

	void onDisable(){

		GameLogic.OnDarkSideChanged -= StopGame;
	}

	void OnTriggerExit(Collider other){

		if (other.tag == "Enemy") {
			OnPlayerDead ();
		}
	}

	void Update(){

		if (Input.GetKey (KeyCode.RightArrow)) {
			Vector3 position = this.transform.position;
			position.z--;
			if (position.z > boundary.zMin) {
				this.transform.position = position;
			}
		} else if (Input.GetKey (KeyCode.LeftArrow)) {
			Vector3 position = this.transform.position;
			position.z++;
			if (position.z < boundary.zMax) {
				this.transform.position = position;
			}
		}

		if (Time.time - delay > lastShot) {
			//shoot arrow
			Instantiate(arrow, arrowSpawn.position, arrowSpawn.rotation);

			lastShot = Time.time;
		}
	}

	void StopGame(){
		gameObject.SetActive (false);
	}

}