﻿using UnityEngine;
using System.Collections;

public class ShotMover : MonoBehaviour {

	static float sRATE = 0.05f;

	float timeToGo;

	void Start(){
		timeToGo = Time.fixedTime + sRATE;
	}

	void FixedUpdate(){
		if (Time.fixedTime >= timeToGo) {
			Vector3 position = gameObject.transform.position;
			position.y--;
			gameObject.transform.position = position;

			timeToGo = Time.fixedTime + sRATE;
		}
	}
}
