﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class GameLogic : MonoBehaviour {

	public GameObject spaceInvaderCanvas;
	public GameObject enemyLevel1;
	public GameObject enemyLevel2;
	public GameObject player;
	public GameObject explosion;

	public delegate void DeathAction();
	public static event DeathAction OnDarkSideChanged;
	public static event DeathAction OnGoodSideChanged;

	static float sRATE = 10.0f;
	float timeToGo = sRATE;

	public static float startTime;

	bool isFirst = true;

	int score = 0;
	int lives = 3;

	int curLevel = 1;
	int curEnemyDiedCount = 0;

	int enemiesLevel1 = 13;
	int enemiesLevel2 = 5;
		
	void OnEnable(){
		EnemyController.OnEnemyDead += IncreaseScore;
		PlayerController.OnPlayerDead += DecreaseLives;
	}

	void onDisable(){
		EnemyController.OnEnemyDead -= IncreaseScore;
		PlayerController.OnPlayerDead -= DecreaseLives;
	}

	void Start(){
		LoadLevel1 ();
	}

	bool isUserPresent=true;
	void Update()
	{
		if (isFirst) {
			if (Time.time >= timeToGo) {
				SwitchedToDarkSide ();
			}
		}

//		if (!OVRPlugin.userPresent && isUserPresent) {
//			SceneManager.LoadScene (0);
//			isUserPresent = false;
//		}
	}

	void SwitchedToGoodSide(){
		//do not switch again to the dark side
		isFirst = true;

		startTime = Time.time;
		timeToGo = startTime + sRATE;

		Color color = new Color ();
		//change ambient light
		ColorUtility.TryParseHtmlString ("#4B2D00", out color);
		RenderSettings.ambientLight = color;
		//change the muralart and remove the space invaders objects
		OnGoodSideChanged();
		//explosion.SetActive (false);

		LoadLevel1 ();
	}

	void SwitchedToDarkSide(){
		//do not switch again to the dark side
		isFirst = false;
		Color color = new Color ();
		//change ambient light
		ColorUtility.TryParseHtmlString ("#510f00", out color);
		RenderSettings.ambientLight = color;
		//change the muralart and remove the space invaders objects
		OnDarkSideChanged();
		//remove the canvas from the view
		EnableCanvas (false);
		explosion.SetActive (true);

		Debug.Log ("Switch to dark side");
	}

	void IncreaseScore(){
		Text[] children = spaceInvaderCanvas.GetComponentsInChildren<Text> ();

		for (int i = 0; i < children.Length; i++) {
			//go through the elements to find the scorecount element to increase the score
			if (children [i].name.Contains ("ScoreCount")) {
				Text scoreCount = children [i];
				score += 10;
				curEnemyDiedCount++;
				scoreCount.text = score.ToString();
			}
		}

		if (curLevel == 1) {
			//if all enemies are destroyed switch to other view
			if (curEnemyDiedCount == enemiesLevel1) {
				LoadLevel2 ();
			}
		} else {
			//if a number of enemies are destroyed switch to the dark side
			if (curEnemyDiedCount == enemiesLevel2) {
				SwitchedToDarkSide ();
			}
		}
	}

	void DecreaseLives(){

		if (lives == 0) {
			//reload game
			if (curLevel == 1) {
				LoadLevel1 ();
			} else if (curLevel == 2) {
				//if the player dies in level 2 the cave will switch to the dark side
				SwitchedToDarkSide ();
			}

		}else {
			Transform[] children = spaceInvaderCanvas.GetComponentsInChildren<Transform> ();
			lives--;
			//go through the elements to remove the lives
			for (int i = 0; i < children.Length; i++) {
				GameObject mrLife = children [i].gameObject;
				if (lives == 2 && mrLife.name.Contains ("Life3")) {
					mrLife.SetActive(false);

				} else if (lives == 1 && mrLife.name.Contains ("Life2")) {
					mrLife.SetActive(false);

				}else if (lives == 0 && mrLife.name.Contains ("Life1")) {
					mrLife.SetActive(false);
				}
			}
		}
	}

	void EnableCanvas(bool isEnable){
		Transform[] children = spaceInvaderCanvas.GetComponentsInChildren<Transform> (true);
		for (int i = 0; i < children.Length; i++) {
			children [i].gameObject.SetActive(isEnable);
		}
		spaceInvaderCanvas.SetActive (isEnable);
	}

	void LoadLevel1(){
		startTime = Time.time;
		timeToGo = startTime + sRATE;

		Debug.Log ("StartTime: "+startTime);
		Debug.Log ("TimeLeft: " + timeToGo);

		curLevel = 1;
		curEnemyDiedCount = 0;
		lives = 3;
		score = 0;

		enemyLevel2.SetActive(false);
		enemyLevel1.SetActive (true);
		ActiveEnemyForLevel (enemyLevel1, true);
		player.SetActive (true);
		EnableCanvas (true);

		Debug.Log ("LOAD LEVEL 1");
	}

	void LoadLevel2(){
		curLevel = 2;
		curEnemyDiedCount = 0;

		ActiveEnemyForLevel (enemyLevel1, false);
		ActiveEnemyForLevel (enemyLevel2, true);
	}

	void ActiveEnemyForLevel(GameObject enemyLevel, bool isEnable){
		Transform[] enemies = enemyLevel.GetComponentsInChildren<Transform> (true);
		for (int i = 0; i < enemies.Length; i++) {
			enemies [i].gameObject.SetActive(isEnable);
		}
	}
}
