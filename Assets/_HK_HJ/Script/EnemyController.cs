﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	public GameObject shot;
	public Transform shotSpawn;
	public int delayMin = 5;
	public int delayMax = 50;

	private float delay;

	private float lastShot;

	private bool isShootingEnabled = true;

	public delegate void DeathAction();
	public static event DeathAction OnEnemyDead;

	void OnTriggerExit(Collider other){
		if(other.name.Contains("Arrow")){
			//event enemy died
			OnEnemyDead ();

			Destroy (other.gameObject);
			RemoveEnemy ();
		}
	}

	void OnEnable(){
		GameLogic.OnDarkSideChanged += RemoveEnemy;
		GameLogic.OnGoodSideChanged += AddEnemy;
	}

	void onDisable(){
		GameLogic.OnDarkSideChanged -= RemoveEnemy;
		GameLogic.OnGoodSideChanged -= AddEnemy;
	}

	void Start(){
		AddEnemy ();
	}

	void Update(){

		if (isShootingEnabled) {
			if (Time.time - delay > lastShot) {
				//shoot arrow
				Instantiate (shot, shotSpawn.position, shotSpawn.rotation);

				randomDelay ();
				lastShot = Time.time;
			}
		}
	}

	private void randomDelay(){
		System.Random random = new System.Random ();
		delay = delayMin + random.Next () % (delayMax + 1);
	}

	void RemoveEnemy(){
		isShootingEnabled = false;
		gameObject.SetActive(false);
	}

	void AddEnemy(){

		lastShot = Time.time;
		randomDelay ();

		isShootingEnabled = true;
		gameObject.SetActive (true);
	}
}
