﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class GazeOnScreen : EventTrigger {

	Collider coll;

	// Use this for initialization
	void Start () {
		coll = GetComponent<Collider> ();
	}


	public override void OnPointerEnter(PointerEventData data)
	{
		Debug.Log (data.pointerCurrentRaycast.worldPosition.ToString ());
		gameObject.SetActive (false);
	}


}
