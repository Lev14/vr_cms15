﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class DayNightShift : MonoBehaviour {

	Material skyboxMat;

	public RectTransform tvScreen;
	public Light flackern;
	public AudioClip door;
	public List <GameObject> wegfallend;
	private AudioSource source;

	// Use this for initialization
	void Start () {
		skyboxMat = GetComponent<Skybox> ().material;
		StartCoroutine (Ablauf());
		source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		float dayTime = (Mathf.Sin (Time.time * 1f)+1f)/2f;
		skyboxMat.SetFloat ("_Blend", dayTime);
	}

	//Coroutine mit unendlicher Schleife - while (true)
	IEnumerator Ablauf (){
		//reinschreiben was es macht
		//sekunden warten
		yield return new WaitForSeconds(4);
		//neue Aktion
		yield return null;
		//wartet 1 frame

//		GameObject.Find ("door").GetComponent<Animator> ().Play ();


//		GameObject 
		tvScreen.FindChild("Text").GetComponent<Text>().text = "tausend punkte";

		GameObject.Find ("FlackernTV").GetComponent<Light> ().enabled=false;
		yield return new WaitForSeconds(0.5F);
		GameObject.Find ("FlackernTV").GetComponent<Light> ().enabled=true;
		yield return new WaitForSeconds (1);
		Animator animator = GameObject.Find ("door").GetComponent<Animator> ();
		source.PlayOneShot(door,1.0F);
		animator.SetTrigger ("open");
		yield return new WaitForSeconds(3);
		GameObject.Find ("FlackernTV").GetComponent<Light> ().enabled=false;
		yield return new WaitForSeconds(3F);
		GameObject.Find ("FlackernTV").GetComponent<Light> ().enabled=true;
		yield return new WaitForSeconds(0.5F);
		GameObject.Find ("FlackernTV").GetComponent<Light> ().enabled=false;
		yield return new WaitForSeconds (5);

		foreach (GameObject onefallingObj in wegfallend) {
			onefallingObj.SetActive(false);
			//onefallingObj.AddComponent<Rigidbody> ();
			yield return new WaitForSeconds (1);
		}

		yield break;
		//geht aus methode raus

		//getComponent light on/off


	}
}
