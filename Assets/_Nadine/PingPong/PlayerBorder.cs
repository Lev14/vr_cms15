﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// This behaviour is attached to the borders behind the players.
/// It controls the collision with the ball.
/// </summary>
public class PlayerBorder : MonoBehaviour {
	
	/// <summary>
	/// Indicates the boder of the left or right player.
	/// </summary>
	public ePlayer player;

	Text scoreText;

	/// <summary>
	/// Current score of the right player.
	/// </summary>
	public static int scorePlayerRight;
	/// <summary>
	/// Current score of the left player.
	/// </summary>
	public static int scorePlayerLeft;

	/// <summary>
	/// Score that must be reached, to win the game.
	/// </summary>
	public static int winningScore = 5;

	void Start()
	{
		scoreText = GameObject.Find ("TVScreen").transform.FindChild ("Canvas").FindChild ("Text").GetComponent<Text> ();
	}


	void Update(){
		//		var rootGameObj = SceneManager.GetSceneByName ("GRZLG").GetRootGameObjects ();

		scoreText.text = scorePlayerLeft + " / " + scorePlayerRight;




		if (scorePlayerLeft >= winningScore || scorePlayerRight >= winningScore)
		{
			// disable ball
			GameObject ball = GameObject.Find("Ball");
			if (ball != null)
			{
				ball.SetActive(false);
			}	

		}

	}
	
	/// <summary>
	/// Invoked by Unity if a GameObject collides with this GameObject.
	/// </summary>
	void OnCollisionEnter(Collision col)
	{
		// Has the GameObject that collides the Ball component?
		Ball ball = col.gameObject.GetComponent<Ball>();
		if (ball != null)
		{
			var screen = GameObject.Find ("TVScreen");
			if (screen) {
				
				// move the ball back to the center of the arena
				ball.GetComponent<Rigidbody>().MovePosition(new Vector3(0f, 1f, -33.75f));

				// increment score of the other player
				if (player == ePlayer.Right) {
					scorePlayerLeft++;
				}
				else {
					scorePlayerRight++;
				}
			}


		}
	}
}
